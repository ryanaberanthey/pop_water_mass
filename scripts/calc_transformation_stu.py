from parallel_analysis import wmt_rho
import os

# general params
hconst = 50
pref = 2000

# hires version
basedir = '/glade/scratch/sbishop'
runs = {'control': 'hybrid_v5_rel04_BC5_ne120_t12_pop62',
         'perturb': 'bc5.e110_rel04.B_2000_CAM5.ne120_t12.tau_pert.000'}
fsuffix = ''
years = range(55,66)

for run, run_dir in runs.iteritems():
    ddir = os.path.join(basedir, run_dir, 'ocn')
    prefix = run_dir + '.pop.h'
    name = 'stu_%s' % run
    wmt_rho(name, ddir, prefix, years, fsuffix=fsuffix,
        pref=pref, hconst=hconst)
