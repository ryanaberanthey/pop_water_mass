from parallel_analysis import wmt_rho

# general params
hconst = 50
pref = 0

# hires version
basedir = '/glade/scratch/asolomon/WCM'
fsuffix = ''
years = range(1955,2006)


runs = {'fix': range(1,7), 'wcm': range(2,8)}

for r, ens_num in runs.iteritems():
    for n in ens_num:
        ddir = '%s/%s%1d/' % (basedir,r,n)
        if r == 'fix':
            fprefix = 'b.e10.B55TRWCN.f19_g16.fixODS1955.%03d.pop.h' % n
        elif r == 'wcm':
            fprefix = 'b40.1955-2005.2deg.wcm.%03d.pop.h' % n
        name = 'ari_%s%1d' % (r,n)
        wmt_rho(name, ddir, fprefix, years, fsuffix=fsuffix,
            pref=pref, hconst=hconst)
