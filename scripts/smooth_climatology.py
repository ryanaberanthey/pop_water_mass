import numpy as np
from astropy.convolution import convolve, Gaussian2DKernel
from watermasstools import pop_model

ddir = '/glade/p/work/rpa/pop/hybrid_v5_rel04_BC5_ne120_t12_pop62_DERIVED/'

for n in range(1,13):
    fname = ddir + 'climatology.%02d.nc' % n
    print fname
    p = pop_model.POPFile(fname)
    print 'calculating rho...'
    rho = np.ma.masked_array(p.rho[0], p.mask)
    print 'smoothing...'
    gauss_kernel = Gaussian2DKernel(10)
    rho_smooth = np.ma.masked_array(
                convolve(rho.filled(np.nan), gauss_kernel, boundary='wrap'),
              p.mask)
    np.savez(ddir + 'sigma0_smooth.%02d' % n,
             rho=rho_smooth.filled(0.), mask=p.mask)

